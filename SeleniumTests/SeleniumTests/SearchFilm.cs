﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Security.AccessControl;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;

namespace SeleniumTests
{
    [TestFixture]
    class SearchFilm
    {
        private IWebDriver driver;
       /* [OneTimeSetUp]
        public void InicializeDriver()
        {
            driver = new ChromeDriver();
        }*/

        [SetUp]
        public void OpenMainPage()
        {
            driver = new ChromeDriver();
            driver.Url = "https://www.kinopoisk.ru/";
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(30);

        }
        //Task for lesson 2
        [Test]
        [Category("Smoke")]
        public void Osminogs()
        {
            
            //WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromMinutes(1));


            IWebElement fieldName = driver.FindElement(By.XPath("//form//input[@class='header-search-partial-component__search-field']"));
            fieldName.Clear();
            fieldName.SendKeys("осьминог");
            IWebElement buttonSearch = driver.FindElement(By.XPath("//input[@class='header-search-partial-component__button'][@value= 'искать!']"));
            buttonSearch.Click();
            // Func < IWebDriver, bool> waitForElement = new Func<IWebDriver, bool>((IWebDriver Web)) => ;
            Thread.Sleep(2000);


            IWebElement buttonShowAll = driver.FindElement(By.XPath("//p[@class = 'show_all']//a"));
            buttonShowAll.Click();
            Thread.Sleep(2000);

            IList<IWebElement> films = driver.FindElements(By.XPath("//div[@class='element']//a[@class= 'js-serp-metrika'and contains(., 'осьминог')]"));

            for (int i = 0; i < films.Count; ++i)
            {

                IWebElement film = films[i];

                film.SendKeys(OpenQA.Selenium.Keys.Control+ OpenQA.Selenium.Keys.Shift + OpenQA.Selenium.Keys.Return);
                Thread.Sleep(2000);

                var newWindow = driver.SwitchTo().Window(driver.WindowHandles[1]);
                String name = driver.FindElement(By.XPath("//div[@id= 'headerFilm']//h1[@class= 'moviename-big']")).Text;

                try
                {
                    IWebElement disc = driver.FindElement(By.XPath("//td[@class= 'news']//span[@class= '_reachbanner_']"));

                    if (disc.Displayed)
                    {
                        var discription = driver.FindElement(By.XPath("//td[@class= 'news']//span[@class= '_reachbanner_']")).Text;
                        Console.WriteLine("Description for film "+"'"+ name+"'"+ ": "+ discription);

                    }
                }
                catch (Exception)
                {
                    Console.WriteLine("Description for film "+"'"+name+"'"+" is absent!");
                }

                newWindow.Close();
                driver.SwitchTo().Window(driver.WindowHandles[0]);

            }
        }

        //Task for lesson 3
        [TestCase("кактус")]
        [TestCase("осьминог")]
        public void Search(string movie)
        {

            IWebElement fieldName = driver.FindElement(By.XPath("//form//input[@class='header-search-partial-component__search-field']"));
            fieldName.Clear();
            fieldName.SendKeys(movie);
            IWebElement buttonSearch = driver.FindElement(By.XPath("//input[@class='header-search-partial-component__button'][@value= 'искать!']"));
            buttonSearch.Click();
            Thread.Sleep(2000);


            IWebElement buttonShowAll = driver.FindElement(By.XPath("//p[@class = 'show_all']//a"));
            Assert.True(buttonShowAll.Displayed);

            buttonShowAll.Click();
            Thread.Sleep(2000);

            IList<IWebElement> films = driver.FindElements(By.XPath("//div[@class='element']//a[@class= 'js-serp-metrika'and contains(., '" + movie + "')]"));

            for (int i = 0; i < films.Count; ++i)
            {

                IWebElement film = films[i];

                film.SendKeys(OpenQA.Selenium.Keys.Control + OpenQA.Selenium.Keys.Shift + OpenQA.Selenium.Keys.Return);
                Thread.Sleep(2000);

                var newWindow = driver.SwitchTo().Window(driver.WindowHandles[1]);
                String name = driver.FindElement(By.XPath("//div[@id= 'headerFilm']//h1[@class= 'moviename-big']")).Text;
                Assert.True(name.Contains(movie));

                var view = driver.FindElement(By.XPath("//div[@class= 'block_add extended']//div[@class='select']")).Text;
                Assert.AreEqual("Буду смотреть", view);

                try
                {
                    IWebElement disc = driver.FindElement(By.XPath("//td[@class= 'news']//span[@class= '_reachbanner_']"));

                    if (disc.Displayed)
                    {
                        var discription = driver.FindElement(By.XPath("//td[@class= 'news']//span[@class= '_reachbanner_']")).Text;
                        Console.WriteLine("Description for film " + "'" + name + "'" + ": " + discription);

                    }
                }
                catch (Exception)
                {
                    Console.WriteLine("Description for film " + "'" + name + "'" + " is absent!");
                }

                newWindow.Close();
                driver.SwitchTo().Window(driver.WindowHandles[0]);

            }
        }


        [TearDown]
        public void AfterTest()
        {
            driver.Quit();
        }
    }
}
